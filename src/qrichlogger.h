#pragma once
#include <QtCore/QObject>
#include <memory>


enum LogSeverity {
    LOG_EMERG    = 0,    /* application is unusable          */
    LOG_ALERT    = 1,    /* action must be taken immediately */
    LOG_CRITICAL = 2,    /* critical conditions              */
    LOG_ERROR    = 3,    /* error conditions                 */
    LOG_WARNING  = 4,    /* warning conditions               */
    LOG_NOTICE   = 5,    /* normal but significant condition */
    LOG_INFO     = 6,    /* informational                    */
    LOG_DEBUG    = 7     /* debug-level messages             */
};


class Logger : public QObject
{
public:
    static Logger&     get();
    const  std::FILE * file() const;

    void log(LogSeverity severity, const char * format, va_list ap);

private:
    Logger();
    ~Logger();

    std::FILE* file_;

signals:
    void sigLog(LogSeverity severity, std::shared_ptr<const char*> message);
};


void  LOG(LogSeverity severity, const char * format, ...);
void VLOG(LogSeverity severity, const char * format, va_list ap);

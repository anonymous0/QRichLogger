#include "qrichlogger.h"
#include <chrono>
#include <cstdarg>
#include <cstdio>
#include <ctime>
#include <stdexcept>
#include <iostream>


const char * LogSeverityStrings[] = {
    "EMERG",
    "ALERT",
    "CRITICAL",
    "ERROR",
    "WARNING",
    "NOTICE",
    "INFO",
    "DEBUG"
};


Logger::Logger()
{
    file_ = std::tmpfile();
}


Logger::~Logger()
{
    std::fclose(file_);
}


Logger& Logger::get()
{
    static Logger instance;
    return instance;
}


const std::FILE * Logger::file() const
{
    return file_;
}



void Logger::log(LogSeverity severity, const char* format, va_list ap)
{
    va_list ap_console;
    va_copy(ap_console, ap);

    auto now = std::chrono::system_clock::now();
    time_t tt   = std::chrono::system_clock::to_time_t(now);
    tm * t = localtime(&tt);
    auto fraction  = now.time_since_epoch();
    fraction -= std::chrono::duration_cast<std::chrono::seconds>(fraction);

    char time_str[100];
    std::sprintf(time_str, "%04u-%02u-%02u %02u:%02u:%02u.%03u ", t->tm_year + 1900,
                t->tm_mon + 1, t->tm_mday, t->tm_hour, t->tm_min, t->tm_sec,
                static_cast<unsigned>(fraction / std::chrono::milliseconds(1)));

    std::fprintf(file_, "%s", time_str);
    std::fprintf(file_, "[%s] ", LogSeverityStrings[severity]);
    std::vfprintf(file_, format, ap);
    std::fprintf(file_, "\n");

    std::fprintf(stderr, "%s", time_str);
    std::fprintf(stderr, "[%s] ", LogSeverityStrings[severity]);
    std::vfprintf(stderr, format, ap_console);
    std::fprintf(stderr, "\n");
    va_end(ap_console);
}


void LOG(LogSeverity severity, const char * format, ...)
{
    auto& logger = Logger::get();
    va_list ap;
    va_start (ap, format);
    logger.log(severity, format, ap);
    va_end (ap);
}


void VLOG(LogSeverity severity, const char * format, va_list ap)
{
    auto& logger = Logger::get();
    logger.log(severity, format, ap);
}
